# Maven

Maven est un outil de gestion de projet. 

Il permet d'assurer la gestion des dépendances et de standardiser le processus de build en plusieurs phases.

J'ai préparé pour vous ce projet qui vous permet:
- D'écrire des tests avec Junit 4
- De vérifier le coverage de votre code
- D'éxecuter le projet

**/!\ vous devez avoir un JDK 8 mininum installé pour pouvoir utilisé ce projet**


## Exécution de maven
J'ai téléchargé pour vous maven à la racine de ce projet.

## Linux / Macos

Pour éxecuter maven sur linux/macos vous devez vous rendre dans le répertoire du projet un terminal.

Vous pouvez éxecuter la commande suivante pour éxecuter les tests:
```bash
./maven/bin/mvn test
```

## Windows
Pour éxecuter sur windows vous devez vous rendre dans le répertoire du projet un terminal.

```bash
.\maven\bin\mvn.cmd test
```

## Les différentes commandes dont vous aurez besoin

### Exécuter les tests et le coverage:
```bash
./maven/bin/mvn test
```

Le résultat du coverage se trouve dans le répertoire `./target/site/jacoco/index.html`, c'est un fichier html que vous pouvez ouvrir dans n'importe quel navigateur.


### Éxecuter la classe main
```bash
./maven/bin/mvn exec:java
```